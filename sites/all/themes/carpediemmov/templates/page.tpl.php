<?php 
/**
 * @file
 * Alpha's theme implementation to display a single Drupal page.
 */
?>

<div<?php print $attributes; ?>>
  <?php if (isset($page['header'])) : ?>
    <?php print render($page['header']); ?>
  <?php endif; ?>
  
  <?php if (isset($page['content'])) : ?>
    <?php print render($page['content']); ?>
  <?php endif; ?>  
  
  <?php if (isset($page['footer'])) : ?>
    <?php print render($page['footer']); ?>
  <?php endif; ?>
</div>
<script>
jQuery( document ).ready(function() {
//Reziseo INICIAL de Pantalla Principal //Si es mayor el width a 785 y si el ancho es mayor al largo
	if(jQuery(window).width() >782 ){
		jQuery("#block-views-view-menu-testimonios-block").insertBefore(".node-type-testimonios #region-content .region-inner");
	}
	else{
		jQuery("#block-views-view-menu-testimonios-block").insertAfter(".node-type-testimonios #region-content .region-inner");
	}
	
//Reziseo de Pantalla Principal //Si es mayor el width a 785 y si el ancho es mayor al largo
jQuery(window).resize(function() {
	if(jQuery(window).width() >782 ){
		jQuery("#block-views-view-menu-testimonios-block").insertBefore(".node-type-testimonios #region-content .region-inner");
	}
	else{
		jQuery("#block-views-view-menu-testimonios-block").insertAfter(".node-type-testimonios #region-content .region-inner");
	}
});//Jquery.resize()*/
	
});
</script>